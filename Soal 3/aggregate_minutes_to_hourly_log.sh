#!/bin/bash

i=59
jam=$(date +"%H")
jam=$((jam-1))

while [ $i -ge 0 ]
do
if [ $i -ge 10 ]
then
tanggal=$(date +"%Y%m%d$jam$i%S")
bulan=$(date +"%Y%m%d%H")
metrics_$tanggal.log >> $HOME/log/metrics_agg_$bulan.log
else
tanggal=$(date +"%Y%m%d$jam0$i%S")
bulan=$(date +"%Y%m%d%H")
metrics_$tanggal.log >> $HOME/log/metrics_agg_$bulan.log
fi
i=$((i-1))
done
