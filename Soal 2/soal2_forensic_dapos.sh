#A
#pada poin ini kita diminta untuk membuat directory, perintah ini bertugas untuk membuat file directory jika directory belum ada.
mkdir "forensic_log_website_daffainfo_log"

#B
#Pada poin ini diminta menghitung rata - rata serangan tiap jam
awk -F: 'NR>1{print $3}'  log_website_daffainfo.log | sort | uniq -c | awk '{avg+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", avg/NR}' > forensic_log_website_daffainfo_log/ratarata.txt

#C
#Pada poin ini diminta untuk mencari IP yang paling banyak melakukan request ke server dan menampilkan banyak request yang dikirim dari IP tersebut.
awk -F: 'NR>1{print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk 'NR==1{gsub(/"/,"")}1' | awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' > forensic_log_website_daffainfo_log/result.txt 

#D
#Pada poin ini diminta menghitung banyaknya request yang menggunakan curl sebagai user-agent
awk -F':"' '/curl/ {print $4}' log_website_daffainfo.log | awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> forensic_log_website_daffainfo_log/result.txt

#E
#Pada poin ini diminta untuk menampilkan semua request yang dilakukan pada jam 2 pagi.
awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' log_website_daffainfo.log | sort | awk '{gsub(/"/,"")}1' | awk '/22\/Jan\/2022:02:/ {print $1}' | uniq | awk '{print}' >> forensic_log_website_daffainfo_log/result.txt
