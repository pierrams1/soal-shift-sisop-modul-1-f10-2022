login_fun(){
	if [[ ! -f "$localuser" ]]
	then
		echo "No user registered yet"
	else
		if grep -q -w "$username $pass" "$localuser"
		then
			echo "$calendar $time LOGIN:INFO User $username logged in" >> $localog
			echo "Login success"

			printf "Enter command [dl or att]: "
			read command
			if [[ $command == att ]]
			then
				att_fun
			elif [[ $command == dl ]]
			then
				donpic_fun
			else
				echo "Not found"
			fi

		else
			fail="Failed login attemp on user $username"
			echo $fail

			echo "$calendar $time LOGIN:ERROR $fail" >> $localog
		fi
	fi
}

passcheck_fun(){
	local passlength=${#pass}

	if [[ $passlength -lt 8 ]]
    	then
        	echo "password must be more than 8 characters"

	elif [[ "$pass" != *[[:upper:]]* || "$pass" != *[[:lower:]]* || "$pass" != *[0-9]* ]]
	    then
       		echo "password must be at least upper, lower and number!"	
        else
		login_fun
	fi
}

unzip_fun(){
	unzip -P $pass $folder.zip
	rm $folder.zip

	count=$(find $folder -type f | wc -l)
	donstart_fun
}

donstart_fun(){
	for(( i=$count+1; i<=$n+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip -P $pass -r $folder.zip $folder/
	rm -rf $folder
}
donpic_fun(){
	printf "Enter number: "
	read n

	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		count=0
		donstart_fun
	else
		unzip_fun
	fi

}

att_fun(){
	if [[ ! -f "$localog" ]]
	then
		echo "log not found"
	else
		awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' $localog
	fi
}

calendar=$(date +%D)
time=$(date +%T)
folder=$(date +%Y-%m-%d)_$username
localog=/home/ghazzi/sisop/modul1/soal1/log.txt
localuser=/home/ghazzi/sisop/modul1/soal1/users/user.txt

printf "enter username> "
read username

printf "Enter your password> "
read -s pass



passcheck_fun
