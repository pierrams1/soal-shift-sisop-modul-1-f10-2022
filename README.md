<h3>Soal 1</h3> 
<p>Pertama dijalankan program register untuk membuat akun pada program tersebut. Didalam script program register terdapat 1 fungsi yang berisi melalukan pengecheckan register akun. Pertama fungsi tersebut akan memeriksa apakah terdapat file user.txt jika tidak maka akan dibuat. Setelah itu fungsi akan mengcheck apakah username sudah teregistrasi apa belum, jika sudah maka akan muncul peringatan error “user already exists”. Fungsi ini juga akan memeriksa apakah username dan password yang diregister sama. Selain itu fungsi ini juga memeriksa apakah password memiliki lebih dari 8 karakter dan terdapat huruf kapital, dan angka. Jika semua terpenuhi maka akun tersebut akan teregister dan masuk ke user.txt. Setiap percobaan pembuatan akun akan di record dan dimasukan ke log.txt. lalu pada main terdapat insert username dan insert pass lalu pemanggilan fungsi.(note: pada saat menjalankan program harus menggunakan super user do agar program berjalan dengan baik) </p>

![FOTO1](img/Untitled5.png)

![FOTO1](img/Untitled6.png)

<p>Setelah akun teregister makan akan dijalankan program main.sh untuk mendownload gambar. Pada saat program dijalankan pertama kali maka akan muncul input username dan password. Jika password tidak memenuhi kriteria maka akan muncul peringatan.  
Jika password memenuhi syarat maka akan dicheck apakah akun yang dimasukan teregister atau belum. Jika yang dimasukan telah betul tetapi password yang dimasukan salah maka akan muncul peringatan bahwa login gagal. Jika sudah betul username dan password yang dimasukan akan muncul pilihan antara dl atau att.</p>

![FOTO1](img/Untitled7.png)

<p>Jika password memenuhi syarat maka akan dicheck apakah akun yang dimasukan teregister atau belum. Jika yang dimasukan telah betul tetapi password yang dimasukan salah maka akan muncul peringatan bahwa login gagal. Jika sudah betul username dan password yang dimasukan akan muncul pilihan antara dl atau att</p>

![FOTO1](img/Untitled8.png)

<p>Jika user memilih dl maka program meminta input berapa banyak gambar yang akan didownload. Setelah di input angka program akan berjalan dan mendownload sesuai dengan permintaan user. file yang didownload langsung akan dizip. Tetapi jika user memilih att maka program akan mengecheck berapa banyak percobaan login dan akan menampilkanya.</p>

![FOTO1](img/Untitled9.png)

<p>Pada script main.sh terdapat beberapa fungsi yang digunakan untuk menjalankan program. Seperti mengecheck password, username, download foto, zip dan unzip gambar danmenghitung percobaan login.(note: sama seperti program register pada saat menjalankan program harus menggunakan super user do agar program berjalan dengan baik)</p>

<h2>Soal 2</h2>

<p>Diberikan sebuah log file yang berisi sekumpulan request ke website https://daffa.info yang dilakukan pada tanggal 22 Januari 2022. Pada soal nomer 2 terdapat 5 poin permasalahan<p>

<p>A<p>
Pada poin a diminta kita perlu membuat directory bernama forensic_log_website_daffainfo_log

mkdir "forensic_log_website_daffainfo_log"

perintah tersebut bertugas membuat directory jika directory belum ada.

<p>B<p>
Pada poin b ini kita diminta untuk menghitung rata - rata serangan tiap jam.

awk -F: 'NR>1{print $3}'  log_website_daffainfo.log | sort | uniq -c | awk '{avg+=$1} END {printf "Rata-rata serangan adalah sebanyak %d requests per jam\n", avg/NR}' > forensic_log_website_daffainfo_log/ratarata.txt

Berikut adalah logika dari point B:

-awk -F: 'NR>1{print $3}'  log_website_daffainfo.log Memecah log berdasarkan titik dua (:) mulai dari baris kedua pada log file kemudian mengambil variabel $3
-Hasil yang ditampilkan akan diurutkan secara leksikografis menggunakan syntax sort.
-Dihitung banyaknya kemunculan tiap jam yang berbeda dengan fungsi uniq -c, misal banyak data dengan jam 00 adalah sebanyak 13. Format yang didapatkan dari proses sebelumnya adalah sebagai berikut(hanya sebagai contoh, angka tidak nyata):
-Mencari rata-rata dengan menjumlahkan nilai-nilai yang ada pada kolom 1 kemudian akan dibagi dengan jumlah baris yang ada. Operasi tersebut dapat dilakukan dengan  awk. Hasil operasi awk tersebut dimasukkan ke dalam file rata-rata.txt.

<p>C<p>
Pada poin c diminta untuk mencari IP yang paling banyak melakukan request ke server dan menampilkan banyaknya request yang dikirim dari IP tersebut.

awk -F: 'NR>1{print $1}' log_website_daffainfo.log | sort | uniq -c | sort -rn | awk 'NR==1{gsub(/"/,"")}1' | awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' > forensic_log_website_daffainfo_log/result.txt 

Berikut adalah logika dari point C:

-awk -F: 'NR>1{print $1}' log_website_daffainfo.log Memecah log berdasarkan titik dua (:) mulai dari baris kedua pada log file kemudian mengambil variabel $1
-| sort  berfungsi untuk melakukan pengurutan secara leksikografis terhadap hasil awk sebelumnya
-| uniq -c berfungsi untuk menghitung banyak kemunculan tiap IP yang berbeda
-| sort -rn berfungsi untuk mengurutkan secara menurun berdasarkan banyaknya kemunculan tiap IP
-awk 'NR==1{gsub(/"/,"")}1' berfungsi untuk menghapus semua petik dua pada baris pertama
-| awk 'NR==1{printf "IP yang paling banyak mengakses server adalah:  %s sebanyak %s requests\n\n", $2, $1}' > forensic_log_website_daffainfo_log/result.txt Mengambil baris pertama (IP paling sering muncul) dengan $1 adalah banyak kemunculan dan $2 adalah IP tersebut. Selanjutnya hasil dimasukkan ke dalam file result.txt

<p>D<p>
Pada poin ini diminta untuk menghitung banyaknya request yang menggunakan curl sebagai user-agent

awk -F':"' '/curl/ {print $4}' log_website_daffainfo.log | awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> forensic_log_website_daffainfo_log/result.txt

Berikut adalah logika dari point D:

-awk -F':"' '/curl/ {print $4}' log_website_daffainfo.log Memecah log berdasarkan titik dua diikuti petik dua (:") ambil semua $4 yang mengandung curl
-| awk 'END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n", NR}' >> forensic_log_website_daffainfo_log/result.txt Hasil dari operasi sebelumnya dihitung barisnya dengan variabel bawaan NR, kemudian ditampilkan sesuai format pada soal. Setelah itu ditambahkan pada result.txt

<p>E<p>
Pada poin E ini kita diminta untuk menampilkan semua request yang dilakukan pada jam 2 pagi.

awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' log_website_daffainfo.log | sort | awk '{gsub(/"/,"")}1' | awk '/22\/Jan\/2022:02:/ {print $1}' | uniq | awk '{print}' >> forensic_log_website_daffainfo_log/result.txt

Berikut adalah logika dari point E:

-awk -F':"' 'NR>1{printf "%s %s\n", $1, $2}' log_website_daffainfo.log yang memisahkan log berdasarkan titik dua diikuti petik dua (:") kemudian mengambil $1 dan $2
-| sort berfungsi untuk mengurutkan hasil dari operasi sebelumnya secara leksikografis
-| awk '{gsub(/"/,"")}1' Berfungsi untuk menghapus semua titik dua
-| awk '/22\/Jan\/2022:02:/ {print $1}' Mengambil semua baris yang mengandung 22/Jan/2022:02: kemudian mengambil $1 yaitu IP
-| uniq Memisahkan IP yang unik, dengan itu tidak akan ada IP yang sama muncul dua kali
-awk '{print}' >> forensic_log_website_daffainfo_log/result.txt Memasukkan hasil kedalam result.txt



<h3>Soal 3</h3>

<p>Membuat Program Monitoring Resource</p>

![FOTO1](img/Untitled.png)
 
<p>Sistem ini akan memonitoring ram computer kita dan juga desktop
Dimana akan terdapat dua program yaitu minutes_log.sh untuk mendapatkan hasil monitoring computer setiap menit, dan aggregate_minutes_to_hourly_log.sh untuk mengumpulkan hasil monitoring computer setiap menit dalam satu jam kemudian menampilkan nilai minimum, maksimum, dan rata-rata dari setiap metrics.</p>

![FOTO1](img/Untitled1.png)
![FOTO1](img/Untitled2.png)

Program minutes_log.sh bertujuan untuk membuat metric tiap menitnya dalam file metric_{YmdHMS}.log

ram=”$(free -m)”

perintah tersebut untuk mendapatkan hasil monitoring ram

disk=$(du -sh $HOME)

perintah untuk mendapatkan hasil monitoring /home/{user}

echo $disk >> $HOME/log/$tanggal.log

akan mengirimkan data pada $disk ke $HOME/log/$tanggal.log

![FOTO1](img/Untitled3.png)
 
Program aggregate_minutes_to_hourly_log.sh untuk mengambil data dari metrics lalu diambil untuk diolah menjadi nilai minimum, maksimum, dan rata-rata.

While [$i -ge 0]

Perulangan dengan kondisi bila nilai i lebih besar atau sama dengan nol

If [$i -ge 10]

tanggal=$(date +”%Y%m%d$jam$i%S”)

bulan=$(date +”%Y%m%d%H”)

metric_tanggal.log >> $HOME/log/metrics_agg_$bulan.log

<p>kondisi bila i lebih besar dari atau sama dengan 10 maka Tindakan yang diambil adalah membuat variable tanggal dan bulan dimana tanggal akan bernilai tahun,bulan,tanggal,nilai jam,nilai i, dan detik secara berurut dan bulan akan bernilai tahun,bulan,tanggal,jam secara berurutan.</p>

Lalu apapun yang ada pada metric_tanggal.log akan dikirimkan ke $HOME/log/metrics_agg_bulan.log

Else

tanggal=$(date +”%Y%m%d$jam0$i%S”)

bulan=$(date +”%Y%m%d%H”)

metric_tanggal.log >> $HOME/log/metrics_agg_$bulan.log

<p>kondisi bila i lebih besar dari atau sama dengan 10 maka Tindakan yang diambil adalah membuat variable tanggal dan bulan dimana tanggal akan bernilai tahun,bulan,tanggal,nilai jam,nilai i dengan nilai 0 diawalan nya, dan detik secara berurut dan bulan akan bernilai tahun,bulan,tanggal,jam secara berurutan.</p>

Lalu apapun yang ada pada metric_tanggal.log akan dikirimkan ke $HOME/log/metrics_agg_bulan.log

Program berakhir dengan error akibat dari $jam0.

![FOTO1](img/Untitled4.png)

Kendala
1.	Banyak typo diawal pengerjaan sehingga banyak waktu yang terbuang dalam mencari kesalahan dan melakukan perbaikan
Contoh: variable=nilai berbeda dengan variable = nilai.
2.	Kebingungan dalam $(date +”%Y%m%d$jam0$i%S”)
Nilai dari $jam0 dianggap satu sehingga nilai jam tidak muncul. Hal ini belum dapat diselesaikan
3.	Belum membuat agregasi
4.	Belum membuat nya otomatis.
